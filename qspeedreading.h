#ifndef QSPEEDREADING_H
#define QSPEEDREADING_H

#include <QLabel>
#include <QPushButton>
#include <QSettings>
#include <QSlider>
#include <QTextEdit>
#include "SpeedReaderCore/speedreader.h"
#include "SpeedReaderLabelWidgetBased/speedreaderlabel.h"

class QDoubleTextEdit : public QWidget
{
    Q_OBJECT

public:
    QDoubleTextEdit(QWidget *parent = 0);

protected:
    void wheelEvent(QWheelEvent *e);

private:
    QTextEdit *text1, *text2;

};

class QTickWidget : public QWidget
{
    Q_OBJECT

public:
    QTickWidget(QWidget *parent = 0);
};

class QSpritzReader : public QWidget
{
    Q_OBJECT

public:
    QSpritzReader(QWidget *parent = 0);
    QSettings settings;

private:
    QLabel *amountLabel,*progressLabel,*realSpeedLabel,*speedLabel,*timeLabel;
    QPushButton *startButton, *stopButton, *prevButton, *nextButton, *resetButton;
    QSlider *speedSlider;
    SpeedWordReader *pSpeedReader;
    SpeedReaderLabel *pSpeedReaderLabel;

public slots:
    void processError(SpeedReaderError error);
    void processEndOfBook();
    void processReadingProgress(double p, int q, double r);
    void writeToSpeedLabel(int a);

private slots:
    void reset();
    void start();
    void stop();
};

class QSpeedTextEdit : public QWidget
{
    Q_OBJECT

public:
    QSpeedTextEdit(QWidget *parent = 0);

private:
    QTextEdit *text;
};

#endif // QSPEEDREADING_H
