#include <QApplication>
#include "qanalogclock.h"
#include "qspeedreading.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    QTabWidget tabWidget;
    QAnalogClock *clock = new QAnalogClock(&tabWidget);
    QDoubleTextEdit *textEdit = new QDoubleTextEdit(&tabWidget);
    QSpritzReader *reader = new QSpritzReader(&tabWidget);
    QSpeedTextEdit *text = new QSpeedTextEdit(&tabWidget);
    reader->show();
    tabWidget.addTab(clock, "clock");
    tabWidget.addTab(textEdit, "text");
    tabWidget.addTab(reader, "spritz");
    tabWidget.addTab(text, "text");
    tabWidget.show();
    return app.exec();
}
