#include <QtGui>
#if(QT_MAJOR_VERSION>4)
    #include <QtWidgets>
#endif
#include <QColorDialog>
#include "qanalogclock.h"

QAnalogClock::QAnalogClock(QTabWidget *parent) : QWidget(parent)
{
    _parent = parent;

    QTimer *timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(update()));
    timer->start(50);

    readSettings();
    hourBrushColor = hourColor;
    hourBrushColor.setAlpha(127);
    minuteBrushColor = minuteColor;
    minuteBrushColor.setAlpha(127);
    secondBrushColor = secondColor;
    secondBrushColor.setAlpha(127);

    setBackgroundColor();
    setMenu();

    setWindowTitle(tr("Analog Clock"));
    resize(200, 200);
}

void QAnalogClock::readSettings()
{
    QSettings settings("sanblch","qanalogclock");
    settings.beginGroup("/Colors");
    if(settings.contains("/background"))
        backgroundColor = settings.value("/background").value<QColor>();
    else
        backgroundColor.setRgb(255,255,255);
    if(settings.contains("/hour"))
        hourColor = settings.value("/hour").value<QColor>();
    else
        hourColor.setRgb(0,0,0);
    if(settings.contains("/minute"))
        minuteColor = settings.value("/minute").value<QColor>();
    else
        minuteColor.setRgb(0,127,127);
    if(settings.contains("/second"))
        secondColor = settings.value("/second").value<QColor>();
    else
        secondColor.setRgb(255,0,0);
    settings.endGroup();
}

void QAnalogClock::setBackgroundColor()
{
    QPalette pal = palette();
    pal.setColor(backgroundRole(),backgroundColor);
    setPalette(pal);
}

void QAnalogClock::setMenu()
{
    bgColorAction = new QAction(tr("&Background Color"),this);
    hourArrowHideAction = new QAction(tr("&Hour"),this);
    hourArrowHideAction->setCheckable(true);
    hourArrowHideAction->setChecked(true);
    minuteArrowHideAction = new QAction(tr("&Minute"),this);
    minuteArrowHideAction->setCheckable(true);
    minuteArrowHideAction->setChecked(true);
    secondArrowHideAction = new QAction(tr("&Second"),this);
    secondArrowHideAction->setCheckable(true);
    secondArrowHideAction->setChecked(true);
    contextMenu = new QMenu(this);
    arrowColorMenu = contextMenu->addMenu(tr("&Arrow Color"));
    arrowColorMenu->addAction(tr("&Hour"));
    arrowColorMenu->addAction(tr("&Minute"));
    arrowColorMenu->addAction(tr("&Second"));
    contextMenu->addAction(bgColorAction);
    hideMenu = contextMenu->addMenu(tr("&Show Arrow"));
    hideMenu->addAction(hourArrowHideAction);
    hideMenu->addAction(minuteArrowHideAction);
    hideMenu->addAction(secondArrowHideAction);
    connect(arrowColorMenu,SIGNAL(triggered(QAction*)),this,SLOT(changeArrowColor(QAction*)));
    connect(bgColorAction,SIGNAL(triggered()),this,SLOT(changeBackgroundColor()));
    connect(hideMenu,SIGNAL(triggered(QAction*)),this,SLOT(changeShowHideArrow(QAction*)));
}

void QAnalogClock::writeSettings()
{
    QSettings settings("sanblch","qanalogclock");
    settings.beginGroup("/Colors");
    settings.setValue("/background",backgroundColor);
    settings.setValue("/hour",hourColor);
    settings.setValue("/minute",minuteColor);
    settings.setValue("/second",secondColor);
    settings.endGroup();
}

void QAnalogClock::closeEvent(QCloseEvent *event)
{
    writeSettings();
    event->accept();
}

void QAnalogClock::contextMenuEvent(QContextMenuEvent *pe)
{
    contextMenu->exec(pe->globalPos());
}

void QAnalogClock::paintEvent(QPaintEvent *)
{
    static const QPoint hourHand[5] = {
        QPoint(1, 0),
        QPoint(2, -35),
        QPoint(0, -70),
        QPoint(-2, -35),
        QPoint(-1, 0)
    };
    static const QPoint minuteHand[5] = {
        QPoint(1, 0),
        QPoint(1, -50),
        QPoint(0, -90),
        QPoint(-1, -50),
        QPoint(-1, 0)
    };

    int side = qMin(width(), height());
    QTime time = QTime::currentTime();

    QPainter painter(this);
    QBrush backgroundBrush(backgroundColor);
    painter.setBackground(backgroundBrush);
    painter.setRenderHint(QPainter::Antialiasing);
    painter.translate(width() / 2, height() / 2);
    painter.scale(side / 200.0, side / 200.0);

    QPen hourPen;
    hourPen.setColor(hourColor);
    hourPen.setWidth(1);
    painter.setPen(hourPen);
    painter.setBrush(hourBrushColor);

    painter.save();
    if(hourArrowHideAction->isChecked()) {
        painter.rotate(30.0 * ((time.hour() + time.minute() / 60.0 + time.second() / 60.0 / 60.0)));
        painter.drawConvexPolygon(hourHand, 5);
    }
    painter.restore();

    for (int i = 0; i < 12; ++i) {
        painter.drawLine(88, 0, 96, 0);
        painter.rotate(30.0);
    }

    if(hourArrowHideAction->isChecked()) {
        painter.setBrush(hourColor);
        painter.drawEllipse(QPoint(0, 0), 5, 5);
    }

    QPen minutePen;
    minutePen.setColor(minuteColor);
    minutePen.setWidth(1);
    painter.setPen(minutePen);
    painter.setBrush(minuteBrushColor);

    painter.save();
    if(minuteArrowHideAction->isChecked()) {
        painter.rotate(6.0 * (time.minute() + time.second() / 60.0 + time.msec() / 60.0 / 1000.0));
        painter.drawConvexPolygon(minuteHand, 5);
    }
    painter.restore();

    for (int j = 0; j < 60; ++j) {
        if ((j % 5) != 0)
            painter.drawLine(92, 0, 96, 0);
        painter.rotate(6.0);
    }

    if(minuteArrowHideAction->isChecked()) {
        painter.setBrush(minuteColor);
        painter.drawEllipse(QPoint(0, 0), 4, 4);
    }

    QPen secondPen;
    secondPen.setColor(secondColor);
    secondPen.setWidth(1);
    painter.setPen(secondPen);
    painter.setBrush(secondColor);

    painter.save();
    if(secondArrowHideAction->isChecked()) {
        painter.rotate(6.0 * (time.second() + time.msec() / 1000.0));
        painter.drawLine(QPoint(0, 0), QPoint(0, -85));
    }
    painter.restore();

    for (int j = 0; j < 300; ++j) {
        if ((j % 5) != 0)
            painter.drawLine(94, 0, 96, 0);
        painter.rotate(1.2);
    }

    if(secondArrowHideAction->isChecked())
        painter.drawEllipse(QPoint(0, 0), 3, 3);
}

void QAnalogClock::changeArrowColor(QAction *action)
{
    if(action->text() == "&Hour") {
        hourColor = QColorDialog::getColor(hourColor);
        hourBrushColor = hourColor;
        hourBrushColor.setAlpha(127);
    } else if(action->text() == "&Minute") {
        minuteColor = QColorDialog::getColor(minuteColor);
        minuteBrushColor = minuteColor;
        minuteBrushColor.setAlpha(127);
    } else if(action->text() == "&Second") {
        secondColor = QColorDialog::getColor(secondColor);
        secondBrushColor = secondColor;
        secondBrushColor.setAlpha(127);
    }
}

void QAnalogClock::changeBackgroundColor()
{
    backgroundColor = QColorDialog::getColor(backgroundColor);
    setBackgroundColor();
}

void QAnalogClock::changeShowHideArrow(QAction *action)
{
    action->setChecked(action->isChecked());
}
