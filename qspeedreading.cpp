#include <QtCore>
#include <QtGui>
#if QT_VERSION >= 0x050000
    #include <QtWidgets>
#endif
#include "qspeedreading.h"

QDoubleTextEdit::QDoubleTextEdit(QWidget *parent) : QWidget(parent)
{
    text1 = new QTextEdit(this);
    text2 = new QTextEdit(this);
    QHBoxLayout *layout = new QHBoxLayout(this);
    layout->addWidget(text1);
    layout->addWidget(text2);
    setLayout(layout);
}

void QDoubleTextEdit::wheelEvent(QWheelEvent *e)
{
}

QSpritzReader::QSpritzReader(QWidget *parent) : QWidget(parent)
{
    pSpeedReader = new SpeedWordReader(2,15);
    pSpeedReaderLabel = new SpeedReaderLabel(2,15);

    connect(pSpeedReader, SIGNAL(error(SpeedReaderError)), this, SLOT(processError(SpeedReaderError)));
    connect(pSpeedReader, SIGNAL(endOfBook()), this, SLOT(processEndOfBook()));
    connect(pSpeedReader, SIGNAL(readingProgress(double,int,double)), this, SLOT(processReadingProgress(double,int,double)));

    connect(pSpeedReader, SIGNAL(nextWordAvailable(QString, int)), pSpeedReaderLabel, SLOT(processNextWordAvailable(QString, int)));
    connect(pSpeedReader, SIGNAL(wordOffset(int)), pSpeedReaderLabel, SLOT(processWordOffset(int)));

    pSpeedReader->openBook("test.fb2", 0, SpeedReaderEncodings::Window_1251);
    pSpeedReader->setReadingSpeed(500);
    pSpeedReader->setCommaPauseTime(150);
    pSpeedReader->setDotPauseTime(250);

    pSpeedReaderLabel->setSymbolColor("red");

    startButton = new QPushButton("Start");
    stopButton = new QPushButton("Stop");
    stopButton->hide();
    prevButton = new QPushButton("Prev");
    nextButton = new QPushButton("Next");
    resetButton = new QPushButton("Reset");

    speedSlider = new QSlider();
    speedSlider->setMinimum(100);
    speedSlider->setMaximum(5000);
    speedSlider->setOrientation(Qt::Horizontal);
    speedSlider->setTickInterval(100);
    speedSlider->setTickPosition(QSlider::TicksBelow);
    speedSlider->setValue(pSpeedReader->getReadingSpeed());

    progressLabel = new QLabel(QString::number(0) + " %");
    realSpeedLabel = new QLabel(QString::number(0) + " ch/min");
    speedLabel = new QLabel(QString::number(pSpeedReader->getReadingSpeed()) + " ch/min");
    amountLabel = new QLabel(QString::number(pSpeedReader->getAmountChars()) + " char");
    timeLabel = new QLabel(QString::number(pSpeedReader->getTimeElapsed()) + " s");

    QHBoxLayout *labelLayout = new QHBoxLayout();
    labelLayout->addStretch();
    labelLayout->addWidget(pSpeedReaderLabel);
    labelLayout->addStretch();

    QHBoxLayout *speedLayout = new QHBoxLayout();
    speedLayout->addWidget(speedLabel);
    speedLayout->addWidget(progressLabel);

    QHBoxLayout *paramLayout = new QHBoxLayout();
    paramLayout->addWidget(realSpeedLabel);
    paramLayout->addWidget(amountLabel);
    paramLayout->addWidget(timeLabel);

    QHBoxLayout *buttonsLayout = new QHBoxLayout();
    buttonsLayout->addWidget(startButton);
    buttonsLayout->addWidget(stopButton);
    buttonsLayout->addWidget(nextButton);
    buttonsLayout->addWidget(prevButton);

    QHBoxLayout *buttons2Layout = new QHBoxLayout();
    buttons2Layout->addWidget(resetButton);
    buttons2Layout->addStretch();

    QVBoxLayout *mainLayout = new QVBoxLayout();
    mainLayout->addLayout(labelLayout);
    mainLayout->addWidget(speedSlider);
    mainLayout->addLayout(speedLayout);
    mainLayout->addLayout(paramLayout);
    mainLayout->addStretch();
    mainLayout->addLayout(buttonsLayout);
    mainLayout->addLayout(buttons2Layout);

    setLayout(mainLayout);

    connect(resetButton,SIGNAL(clicked()),this,SLOT(reset()));
    connect(startButton,SIGNAL(clicked()),this,SLOT(start()));
    connect(stopButton,SIGNAL(clicked()),this,SLOT(stop()));
    connect(prevButton,SIGNAL(clicked()),pSpeedReader,SLOT(previousWord()));
    connect(nextButton,SIGNAL(clicked()),pSpeedReader,SLOT(nextWord()));
    connect(speedSlider,SIGNAL(valueChanged(int)),pSpeedReader,SLOT(setReadingSpeed(int)));
    connect(speedSlider,SIGNAL(valueChanged(int)),this,SLOT(writeToSpeedLabel(int)));
}

void QSpritzReader::processError(SpeedReaderError error)
{
    switch (error)
    {
        case FileNotFoundError: qDebug() << "File not found"; break;
        case CanNotOpenFileError: qDebug() << "Cannot open file"; break;
        case UndefinedCurrentPositionError: qDebug() << "Undefined current position"; break;
        case NoFileToReadError: qDebug() << "There is no file to read"; break;
        case OverflowCurrentPositionError: qDebug() << "Overflow current position error"; break;
    }
}

void QSpritzReader::processEndOfBook()
{
    qDebug() << "End of book";
}

void QSpritzReader::processReadingProgress(double p, int q, double r)
{
    progressLabel->setText(QString::number(p * (double)100) + " %");
    amountLabel->setText(QString::number(q) + " char");
    timeLabel->setText(QString::number(r)+ " s");
    realSpeedLabel->setText(QString::number(q / r * 60) + " char/min");
}

void QSpritzReader::start()
{
    startButton->hide();
    stopButton->show();
    prevButton->setEnabled(false);
    nextButton->setEnabled(false);
    if(settings.contains("/program/current"))
        pSpeedReader->setCurrentPosition(settings.value("/program/current").toInt());
    pSpeedReader->startReading();
}

void QSpritzReader::stop()
{
    stopButton->hide();
    startButton->show();
    prevButton->setEnabled(true);
    nextButton->setEnabled(true);
    pSpeedReader->stopReading();
    settings.setValue("/program/current",QVariant(pSpeedReader->getCurrentPosition()));
}

void QSpritzReader::writeToSpeedLabel(int a)
{
    speedLabel->setText(QString::number(a) + " ch/min");
}

void QSpritzReader::reset()
{
    int a = 0;
    settings.setValue("/program/current",QVariant(a));
}

QSpeedTextEdit::QSpeedTextEdit(QWidget *parent) : QWidget(parent)
{
    text = new QTextEdit(this);
    QHBoxLayout *layout = new QHBoxLayout();
    layout->addWidget(text);
    setLayout(layout);
}
