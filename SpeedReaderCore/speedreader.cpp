#include "functions.h"
#include "speedreader.h"

SpeedWordReader::SpeedWordReader(int x, int y)
{
    this->reset();

    this->row = x;
    this->col = y;
    this->amountWords = 0;
    this->amountTime = 0;

    connect(&this->pauseTimer, SIGNAL(timeout()), this, SLOT(startReading()));
    connect(&this->readingTimer, SIGNAL(timeout()), this, SLOT(readWord()));
}

void SpeedWordReader::readWord()
{
    this->readPositionPercent = (double)this->currentPosition / this->wordsCount;

    if (this->currentPosition < this->wordsCount)
    {
        QString nextWord = this->words.at(this->currentPosition);
        this->amountWords += nextWord.size();
        this->amountTime += (double) readingTimer.interval() / 1000;
        int shift = 0;

        /*shift = this->maxWordLength - nextWord.count() / 3;

        QString spaces;

        for (int i = 0; i < shift; i++)
        {
            spaces.append(" ");
        }

        nextWord = nextWord.prepend(spaces);*/

        emit this->nextWordAvailable(nextWord, shift);
        emit this->readingProgress(this->readPositionPercent, this->amountWords, this->amountTime);

        /*if (nextWord.contains(",") || nextWord.contains(":") || nextWord.contains(";"))
        {
            readingTimer.stop();
            pauseTimer.start(this->commaPauseTime);
        } else
        {
            if (nextWord.contains(".") || nextWord.contains("!") || nextWord.contains("?") || nextWord.contains("...") || nextWord.contains(".."))
            {
                readingTimer.stop();
                pauseTimer.start(this->dotPauseTime);
            }
        }*/

        this->currentPosition++;
    } else
    {
        this->stopReading();

        emit this->endOfBook();
    }
}

void SpeedWordReader::setWordsToRead(const QStringList &words)
{
    this->words.clear();
    words2frame(words, this->words, row, col);
    this->wordsCount = this->words.count();
}

void SpeedWordReader::setCommaPauseTime(const int &commaPauseTime)
{
    this->commaPauseTime = commaPauseTime;
}

void SpeedWordReader::setDotPauseTime(const int &dotPauseTime)
{
    this->dotPauseTime = dotPauseTime;
}

void SpeedWordReader::setReadingSpeed(const int wordPerMinute)
{
    this->wordPerMinute = wordPerMinute;
}

void SpeedWordReader::setMaxWordLength(const int &maxWordLength)
{
    this->maxWordLength = maxWordLength;
}

void SpeedWordReader::openBook(const QString &filePath, const int &position, const QString &encodingFormat)
{
    if (this->pTextFormatReader != 0)
    {
        delete this->pTextFormatReader;
        this->pTextFormatReader = 0;
    }

    if (filePath.endsWith(".fb2", Qt::CaseInsensitive))
    {
        this->pTextFormatReader = new Fb2Reader(filePath, encodingFormat);
    } else
    {
        if (filePath.endsWith(".txt", Qt::CaseInsensitive))
        {
            this->pTextFormatReader = new TxtReader(filePath, encodingFormat);
        } else
        {
            this->pTextFormatReader = new TxtReader(filePath, encodingFormat);
        }
    }

    connect(this->pTextFormatReader, SIGNAL(error(SpeedReaderError)), this, SIGNAL(error(SpeedReaderError)));
    connect(this->pTextFormatReader, SIGNAL(maxWordLength(int)), this, SLOT(setMaxWordLength(int)));
    connect(this->pTextFormatReader, SIGNAL(maxWordLength(int)), this, SIGNAL(wordOffset(int)));

    this->pTextFormatReader->openBook(pTextFormatReader->getFileName());
    this->setWordsToRead(this->pTextFormatReader->getWords());

    if (position < this->wordsCount)
    {
        this->currentPosition = position;
    } else
    {
        emit this->error(OverflowCurrentPositionError);
        this->currentPosition = 0;
    }
}

void SpeedWordReader::previousWord()
{
    this->stopReading();

    if (this->currentPosition > 0)
    {
        this->currentPosition--;
        this->readPositionPercent = (double) this->currentPosition / this->wordsCount;

        QString nextWord = this->words.at(this->currentPosition);
        int shift = 0;

        /*shift = this->maxWordLength - nextWord.count() / 3;

        QString spaces;

        for (int i = 0; i < shift; i++)
        {
            spaces.append(" ");
        }

        nextWord = nextWord.prepend(spaces);*/

        emit this->nextWordAvailable(nextWord, shift);
        emit this->readingProgress(this->readPositionPercent, this->amountWords, this->amountTime);
    }
}

void SpeedWordReader::nextWord()
{
    this->stopReading();

    if (this->currentPosition < this->wordsCount - 1)
    {
        this->currentPosition++;

        this->readPositionPercent = (double) this->currentPosition / this->wordsCount;

        QString nextWord = this->words.at(this->currentPosition);
        int shift = 0;

        /*shift = this->maxWordLength - nextWord.count() / 3;

        QString spaces;

        for (int i = 0; i < shift; i++)
        {
            spaces.append(" ");
        }

        nextWord = nextWord.prepend(spaces);*/

        emit this->nextWordAvailable(nextWord, shift);
        emit this->readingProgress(this->readPositionPercent, this->amountWords, this->amountTime);
    } else
    {
        emit this->endOfBook();
    }
}

QStringList SpeedWordReader::getWordsToRead() const
{
    return this->words;
}

int SpeedWordReader::getCurrentPosition() const
{
    return this->currentPosition;
}

int SpeedWordReader::getCommaPauseTime() const
{
    return this->commaPauseTime;
}

int SpeedWordReader::getDotPauseTime() const
{
    return this->dotPauseTime;
}

int SpeedWordReader::getReadingSpeed() const
{
    return this->wordPerMinute;
}

double SpeedWordReader::getTimeElapsed() const
{
    return this->amountTime;
}

int SpeedWordReader::getAmountChars() const
{
    return this->amountWords;
}

int SpeedWordReader::getWordsCount() const
{
    return this->wordsCount;
}

void SpeedWordReader::reset()
{
    this->wordsCount = -1;
    this->currentPosition = -1;
    this->readPositionPercent = -1;
    this->commaPauseTime = 500;
    this->dotPauseTime = 1000;
    this->wordPerMinute = 200;
    this->maxWordLength = 40;
    this->pTextFormatReader = 0;
}

void SpeedWordReader::startReading()
{
    if (this->pTextFormatReader != 0)
    {
        switch (this->getCurrentPosition())
        {
            case -1:
            {
                emit this->error(Speedreader::UndefinedCurrentPositionError);

                break;
            }

            default:
            {
                if (!words.isEmpty())
                {
                    pauseTimer.stop();
                    readingTimer.start(60000 / this->wordPerMinute * this->row * this->col);
                }

                break;
            }
        }
    } else
    {
        emit this->error(Speedreader::NoFileToReadError);
    }
}

void SpeedWordReader::stopReading()
{
    if (readingTimer.isActive())
    {
        this->currentPosition--;
        readingTimer.stop();
    }

    if (pauseTimer.isActive())
    {
        pauseTimer.stop();
    }
}

void SpeedWordReader::setCurrentPosition(const int &currPosition)
{
    this->currentPosition = currPosition;
}
