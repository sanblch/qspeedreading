#ifndef SPEEDREADER_H
#define SPEEDREADER_H

#include <QObject>
#include <QTimer>
#include <QStringList>
#include "SpeedReaderCore/TextFormatReaders/textformatreader.h"
#include "SpeedReaderCore/TextFormatReaders/txtreader.h"
#include "SpeedReaderCore/TextFormatReaders/fb2reader.h"
#include "math.h"

using namespace Speedreader;

class SpeedWordReader : public QObject
{
    Q_OBJECT
public:
    explicit SpeedWordReader(int x, int y);

    QStringList getWordsToRead() const;

    int getAmountChars() const;
    int getCurrentPosition() const;
    int getCommaPauseTime() const;
    int getDotPauseTime() const;
    int getReadingSpeed() const;
    double getTimeElapsed() const;
    int getWordsCount() const;

private:
    QStringList words;
    int amountWords, currentPosition, commaPauseTime, dotPauseTime, wordPerMinute, wordsCount, maxWordLength;
    double amountTime;
    int row, col;
    double readPositionPercent;
    QTimer readingTimer, pauseTimer;
    TextFormatReader *pTextFormatReader;

    void reset();

signals:
    void nextWordAvailable(QString, int);
    void readingProgress(double,int,double);
    void wordOffset(int);
    void error(SpeedReaderError);
    void endOfBook();

public slots:
    void setCommaPauseTime(const int &commaPauseTime);
    void setCurrentPosition(const int &currPosition);
    void setDotPauseTime(const int &dotPauseTime);
    void setReadingSpeed(const int wordPerMinute);
    void openBook(const QString &filePath, const int &position = 0, const QString &encodingFormat = SpeedReaderEncodings::UTF_8);

    void previousWord();
    void nextWord();

    void startReading();
    void stopReading();

private slots:
    void setWordsToRead(const QStringList &words);
    void setMaxWordLength(const int &maxWordLength);
    void readWord();
};

#endif // SPEEDREADER_H
