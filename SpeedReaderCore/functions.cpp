#include "functions.h"
#include <QtCore>

void words2frame(const QStringList &in, QStringList &out, int y, int x)
{
    QString buf,str,ystr;
    int count = 0, count2 = 0;
    while(count < in.size())
    {
        if(buf.isEmpty())
            str = in[count++];
        else
            str = buf;
        buf.clear();
        if(count < in.size() && str.size() <= x && str.size() + in[count].length() + 1 <= x)
            while(count < in.size() && str.size() <= x && str.size() + in[count].length() + 1 <= x)
                str += " " + in[count++];
        else {
            buf = str.mid(x,str.length()-x);
            str.remove(x,str.length()-x);
        }
        if(count2 == y) {
            count2 = 0;
            out << ystr;
            ystr.clear();
        }
        ystr += str + "\n";
        count2++;

    }
}
