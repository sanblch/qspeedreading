#ifndef SPEEDREADERENUMS_H
#define SPEEDREADERENUMS_H

#include <QString>

namespace Speedreader
{
    enum SpeedReaderError
    {
        FileNotFoundError,
        CanNotOpenFileError,
        UndefinedCurrentPositionError,
        NoFileToReadError,
        OverflowCurrentPositionError
    };

    class SpeedReaderEncodings
    {
        public:
            static const QString Apple_Roman;
            static const QString Big5;
            static const QString Big5_HKSCS;
            static const QString CP949;
            static const QString EUC_JP;
            static const QString EUC_KR;
            static const QString GB18030_0;
            static const QString IBM_850 ;
            static const QString IBM_866;
            static const QString IBM_874;
            static const QString ISO_2022_JP;
            static const QString ISO_8859_1;
            static const QString ISO_8859_2;
            static const QString ISO_8859_3;
            static const QString ISO_8859_4;
            static const QString ISO_8859_5;
            static const QString ISO_8859_6;
            static const QString ISO_8859_7;
            static const QString ISO_8859_8;
            static const QString ISO_8859_9;
            static const QString ISO_8859_10;
            static const QString ISO_8859_13;
            static const QString ISO_8859_14;
            static const QString ISO_8859_15;
            static const QString ISO_8859_16;
            static const QString Iscii_Bng;
            static const QString Dev;
            static const QString Gjr;
            static const QString Knd;
            static const QString Mlm;
            static const QString Ori;
            static const QString Pnj;
            static const QString Tlg;
            static const QString Tml;
            static const QString JIS_X_0201;
            static const QString JIS_X_0208;
            static const QString KOI8_R;
            static const QString KOI8_U;
            static const QString MuleLao_1;
            static const QString ROMAN8;
            static const QString Shift_JIS;
            static const QString TIS_620;
            static const QString TSCII;
            static const QString UTF_8;
            static const QString UTF_16;
            static const QString UTF_16BE;
            static const QString UTF_16LE;
            static const QString UTF_32;
            static const QString UTF_32BE;
            static const QString UTF_32LE;
            static const QString Windows_1250;
            static const QString Window_1251;
            static const QString Windows_1252;
            static const QString Windows_1253;
            static const QString Windows_1254;
            static const QString Windows_1255;
            static const QString Windows_1256;
            static const QString Windows_1257;
            static const QString Windows_1258;
            static const QString WINSAMI2;
    };
}

#endif // SPEEDREADERENUMS_H
