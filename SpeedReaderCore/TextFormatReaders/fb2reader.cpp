#include "fb2reader.h"
#include<QRegExp>

QStringList Fb2Reader::getWords()
{
    QString textToParse = this->text;

    QRegExp rx("<body>(.*)</body>", Qt::CaseInsensitive);
    rx.indexIn(textToParse);
    return countWords(rx.cap(1).replace(QRegExp("<[a-zA-Z0-9!@#$%^&*()_+-=:;\" '<>,./?]{1,50}>"), " ").replace("  ", " ").replace("\t", " ").replace("\n", " ").replace("- ", "-"));
}
