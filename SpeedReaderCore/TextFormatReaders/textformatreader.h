#ifndef TEXTFORMATREADER_H
#define TEXTFORMATREADER_H

#include <QObject>
#include <QStringList>
#include <QTextCodec>

#include "SpeedReaderCore/speedreaderenums.h"

using namespace Speedreader;

class TextFormatReader: public QObject
{
    Q_OBJECT

    public:
         TextFormatReader(const QString &fileName, const QString &textCodecName);

         virtual QStringList getWords() = 0;
         void openBook(const QString &filePath);
         QString getFileName();

    private:
         QTextCodec *pTextCodec;

    protected:
         QString text;
         QString fileName;

         QStringList countWords(QString text);

    signals:
         void error(SpeedReaderError);
         void maxWordLength(int);
};

#endif // TEXTFORMATREADER_H
