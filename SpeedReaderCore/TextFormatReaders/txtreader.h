#ifndef TXTREADER_H
#define TXTREADER_H

#include "textformatreader.h"

class TxtReader : public TextFormatReader
{
    public:
        TxtReader(const QString &fileName, const QString &textCodecName) : TextFormatReader(fileName, textCodecName) {}

        QStringList getWords();
};

#endif // TXTREADER_H
