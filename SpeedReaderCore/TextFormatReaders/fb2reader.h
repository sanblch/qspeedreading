#ifndef FB2READER_H
#define FB2READER_H

#include "textformatreader.h"

class Fb2Reader : public TextFormatReader
{
    public:
        Fb2Reader(const QString &fileName, const QString &textCodecName) : TextFormatReader(fileName, textCodecName) {}

        QStringList getWords();
};

#endif // FB2READER_H
