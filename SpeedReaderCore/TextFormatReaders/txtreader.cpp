#include "txtreader.h"

QStringList TxtReader::getWords()
{
    QString textToParse = this->text;

    return countWords(textToParse.replace("  ", " ").replace("\n", " ").replace("- ", "-"));
}
