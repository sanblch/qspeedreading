#-------------------------------------------------
#
# Project created by QtCreator 2014-11-24T21:37:20
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = qspeedreading
TEMPLATE = app

SOURCES += main.cpp \
    qanalogclock.cpp \
    qspeedreading.cpp \
    SpeedReaderCore/TextFormatReaders/txtreader.cpp \
    SpeedReaderCore/TextFormatReaders/textformatreader.cpp \
    SpeedReaderCore/speedreader.cpp \
    SpeedReaderLabelWidgetBased/speedreaderlabel.cpp \
    SpeedReaderCore/TextFormatReaders/fb2reader.cpp \
    SpeedReaderCore/speedreaderenums.cpp \
    SpeedReaderCore/functions.cpp

HEADERS  += qanalogclock.h \
    qspeedreading.h \
    SpeedReaderCore/TextFormatReaders/txtreader.h \
    SpeedReaderCore/TextFormatReaders/textformatreader.h \
    SpeedReaderCore/speedreader.h \
    SpeedReaderLabelWidgetBased/speedreaderlabel.h \
    SpeedReaderCore/TextFormatReaders/fb2reader.h \
    SpeedReaderCore/speedreaderenums.h \
    SpeedReaderCore/functions.h

OTHER_FILES += qspeedreading.rc

win32:RC_FILE = qspeedreading.rc

RESOURCES += \
    media.qrc
