#ifndef QANALOGCLOCK_H
#define QANALOGCLOCK_H

#include <QMenu>
#include <QSettings>
#include <QTabWidget>
#include <QWidget>

class QAnalogClock : public QWidget
{
    Q_OBJECT

public:
    QAnalogClock(QTabWidget *parent = 0);

protected:
    void closeEvent(QCloseEvent *event);
    void contextMenuEvent(QContextMenuEvent *pe);
    void paintEvent(QPaintEvent *event);

private:
    QAction *bgColorAction;
    QAction *hourArrowHideAction, *minuteArrowHideAction, *secondArrowHideAction;
    QColor hourColor, minuteColor, secondColor;
    QColor hourBrushColor, minuteBrushColor, secondBrushColor;
    QColor backgroundColor;
    QMenu *arrowColorMenu, *contextMenu, *hideMenu;
    QTabWidget *_parent;
    void readSettings();
    void setBackgroundColor();
    void setMenu();
    void writeSettings();

private slots:
    void changeArrowColor(QAction *action);
    void changeBackgroundColor();
    void changeShowHideArrow(QAction *action);
};

#endif
